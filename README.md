# ubuntu-node-docker

This repo mostly exists because mongoDB not provides ARM64 build for debian (ubuntu only) but node team not provides ubuntu images

In case you want to execute tests on ARM64 gitlab workers with mongodb and node you should use images like that

# Repository

Repository located on https://gitlab.com/adaptivestone/ubuntu-node

# Build

To build image use

```
docker buildx build --platform linux/amd64,linux/arm64 -t registry.gitlab.com/adaptivestone/ubuntu-node:latest --push .
```

Where to find build images?

Docker hub:

```
systerr/ubuntu-node
```

and

Gitlab registry

```
registry.gitlab.com/adaptivestone/ubuntu-node:
```
